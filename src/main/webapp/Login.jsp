<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<style>
form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: red;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
}

button:hover {
	opacity: 0.8;
}

.container {
	padding: 16px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
}
</style>
</head>
<body>
	<div id="header" role="banner"
		style="width: 100%; position: relative; float: left; margin-bottom: 20px;">
		<div style="float: left;">
			<a id="storeLogo"
				title="Used, New, and Out of Print Books - We Buy and Sell - Powell's Books"
				href="https://www.powells.com/"> <img id="imgLogo"
				src="PowellsBookStore.JPG"
				alt="Used, New, and Out of Print Books - We Buy and Sell - Powell's Books"
				style="border-width: 0px; height: 100px;">
			</a>
		</div>
	</div>
	<hr
		style="border: none; height: 2px; color: #ccc; background-color: #ccc; text-align: left; width: 100%; margin-bottom: 20px;" />
	<div style="display: block; width: 100%; float: left;">
		<h1>Login Form</h1>
		<form action="Login" method="post">
			<div class="container">
				<label><b>Email</b></label> <input type="text"
					placeholder="Enter Your Email" name="email" required> <label><b>Password</b></label>
				<input type="password" placeholder="Enter Your Password"
					name="password" required>

				<button type="submit">Login</button>
			</div>

			<div class="container" style="background-color: #f1f1f1">
				<span class="psw"><a href="forgotpassword.jsp">Forgot
						password?</a>&nbsp &nbsp <a href="register.jsp" id="register">New
						User ?</a> </span>
			</div>
		</form>

	</div>
	<div
		style="display: block; width: 100%; float: left; text-align: center; margin-top: 40px; margin-bottom: 40px">
		<b> @ POWELLS.COM </b>
	</div>
</body>
</html>
