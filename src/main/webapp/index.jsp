<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page Book Store</title>
<style>
</style>
</head>
<body>
	<div id="header" role="banner"
		style="width: 100%; position: relative; float: left; margin-bottom: 20px;">
		<div style="float: left;">
			<a id="storeLogo"
				title="Used, New, and Out of Print Books - We Buy and Sell - Powell's Books"
				href="/"> <img id="imgLogo"
				src="PowellsBookStore.JPG"
				alt="Used, New, and Out of Print Books - We Buy and Sell - Powell's Books"
				style="border-width: 0px; height: 100px;">
			</a>
		</div>
		<div id="Login" style="margin-right: 3px; float: right"
			role="navigation" aria-label="utility">
			<b>Hello, | <a id="dnn_dnnLogin_loginLink" title="Login"
				rel="nofollow" href="Login.jsp">Login </a>
			</b>
			<div class="language-object"></div>
		</div>
	</div>
	<hr
		style="border: none; height: 2px; color: #ccc; background-color: #ccc; text-align: left; width: 100%; margin-bottom: 20px;" />
	<div style="display: block; width: 100%; float: left;">
		<div style="display: block">
			<a href="/"> <img alt="Powell's Legendary independent Bookstore"
				style="max-width: 100%; border-style: none;"
				src="HeroHeadlineHomePage.JPG">
			</a>
		</div>
		<div style="display: block">
			<a href="/"> <img alt="Powell's City of Books"
				style="max-width: 100%; border-style: none; margin-bottom: 10px;"
				src="HomePage.JPG">
			</a>
		</div>
	</div>
	<div
		style="display: block; width: 100%; float;left; text-align: center; margin-top: 40px; margin-bottom: 40px">
		<b> @ POWELLS.COM </b>
	</div>
</body>
</html>
